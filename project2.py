"""
project2.py - Use this file to enter your answers, and turn in a printout of this file.

UVA CS 1120
Spring 2016

Project 2 -- Genomes Galore!

My name:    [replace with your name]
My UVA ID:  [replace with your ID]
"""

### You should not need to edit the next five lines:

import sys # for setrecursionlimit
import memoize
import apoe

sys.setrecursionlimit(25000)

### 

AUTHOR = "mst3k"   # Put your UVA ID here

# Problem 1

def nucleotide_complement(base):
    """
   Takes as input a single base represented by one of 'A', 'C', 'T', or 'G' and returns the complement of that base (also a one-letter string). 
    """
    return "A"


# Problem 2

def sequence_complement(sequence):
    """
    Takes as input a string or list of single-letter strings representing a DNA sequence , and returns a string or list of symbols that is 
    the complement of the input sequence. 
    """
    return []


# Example test:
# print (sequence_complement(['A','C','A','T']))
# ['T', 'G', 'T', 'A']

# Problem 3

def count_matches(sequence, base):
    """
    Takes as inputs a list and a value, and outputs the number of elements in the list that match the value. 
    """
    return 0


# Example test:
# print (count_matches(['A','C','A','T'], 'A'))
# 2

# Problem 4

def hamming_distance(seq1, seq2):
    """
    Takes as inputs two lists (or two strings), and outputs the number of positions where the list elements are different.    
    """
    return 0


# Example test:
# print (hamming_distance(['A', 'C', 'A', 'T'], ['A', 'C', 'T', 'A']))
# 2
# print (hamming_distance("erode", "geode"))
# 2

# Problem 5

def edit_distance(x, y):
    """
    Takes as input two lists (or two strings — your code should work for both) and returns the Levenshtein distance between those sequences.
    """
    return 0

# Problem 6

# Calculate the edit distance between APOE_NORMAL and APOE_BAD sequences
# (defined in the import file apoe.py).

# Uncomment the following line and move it so that @memoize.memoize appears directly
# above your "def edit_distance(a, b):" line.

#@memoize.memoize

# print(edit_distance(apoe.APOE_NORMAL, apoe.APOE_BAD))

# If you succeed in computing the edit distance between the two APOE variants, you can replace all the comments after Problem 6 with the answer.